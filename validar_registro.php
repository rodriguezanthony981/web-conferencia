<?php if(isset($_POST['submit'])): ; 
        $nombre = $_POST['nombre'];
        $apellido = $_POST['apellido'];
        $email = $_POST['email'];
        $regalo = $_POST['regalo'];
        $total = $_POST['total_pedido'];
        $fecha = date('Y-m-d H:i:s');

        //Pedidos
        $boletos = $_POST['boletos'];
        $camisas = $_POST['pedido_camisas'];
        $etiquetas = $_POST['pedido_etiquetas'];
        include_once 'includes/funciones/funciones.php';
        $pedido = productos_json($boletos, $camisas, $etiquetas);
        
        //Eventos
        $eventos = $_POST['registro'];
        $registro = eventos_json($eventos);
        
        try 
        {
            require_once('includes/funciones/db_conexion.php');
            $stmt = $conn->prepare("INSERT INTO registrados (nombre_registrado, apellido_registrado, email_registrado, fecha_registro, pases_articulos, talleres_registrados, regalo, total_pagado) VALUES (?,?,?,?,?,?,?,?)");
            $stmt->bind_param("ssssssis", $nombre, $apellido, $email, $fecha, $pedido, $registro, $regalo, $total);
            $stmt->execute();
            $stmt->close();
            $conn->close();
            header('Location: validar_registro.php?exitoso=1');
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
        

        
    //  var_dump($_POST);
    ?>
<?php endif; ?>
<?php include_once 'includes/templates/header.php'; ?>

<section class="seccion contenedor">
    <h2>Resumen Registro</h2>
    
    <?php if(isset($_GET['exitoso'])): 
        if($_GET['exitoso'] == "1"):
            echo "<p>Registro Exitoso</p>";
        endif;
    endif; ?>
    <?php
        include_once 'includes/funciones/funciones.php';
        if(url_completa() == "http://conferenciaweb/validar_registro.php?exitoso=1"):
            try {
                require_once('includes/funciones/db_conexion.php');
                $sql = "SELECT ID_Registrado ,nombre_registrado ,apellido_registrado ,email_registrado ,fecha_registro ,pases_articulos ,talleres_registrados ,regalo, total_pagado ";
                $sql .= " FROM registrados";
                $sql .= " INNER JOIN regalos";
                $sql .= " ON registrados.regalo  = regalos.ID_regalo";
                $sql .= " ORDER BY fecha_registro ASC";
                $resultado = $conn->query($sql);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        endif;    
    ?>
     <?php  $calendario = array();
        while ($registros = $resultado->fetch_assoc()) {
            $ID = $registros['ID_Registrado'];
            $nombre = $registros['nombre_registrado'];
            $apellido = $registros['apellido_registrado'];
            $email = $registros['email_registrado'];
            $fecha = $registros['fecha_registro'];
            $pases = $registros['pases_articulos'];
            $Registros = $registros['talleres_registrados'];
            $regalo = $registros['regalo'];
            $total = $registros['total_pagado'];
        };
        echo $nombre;
        echo $apellido;
        echo $email;
        echo $fecha;
        echo $pases;
        $json_pases = json_decode($pases, true);
        var_dump($json_pases);
        var_dump($json_pases['pase_2dias']);
        echo $Registros;
        $json_registro = json_decode($Registros, true);
        var_dump($json_registro);
        var_dump($json_registro['eventos'][1]);
        echo $regalo;
        echo $total;

    ?>
    
    <?php
        $conn->close();
    ?>
</section>


<?php include_once 'includes/templates/footer.php' ?>
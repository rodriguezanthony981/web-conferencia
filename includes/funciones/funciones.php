<?php 
function productos_json(&$boletos, &$camisas = 0, &$etiquetas = 0){
    $dias = array(0 => 'un_dia', 1 => 'pase_completo', 2 => 'pase_2dias');
    $total_boletos = array_combine($dias, $boletos);
    $json = array();

    foreach($total_boletos as $key => $boletos):
        if((int) $boletos > 0):
            $json[$key] = (int) $boletos;
        endif;
    endforeach;

    $camisas = (int) $camisas;
    
    if($camisas > 0):
        $json['camisas'] = $camisas;
    endif;

    if($etiquetas > 0):
        $json['etiquetas'] = $etiquetas;
    endif;

    return json_encode($json);

}

function eventos_json(&$eventos){
    $eventos_json = array();
    foreach($eventos as $evento):
        $eventos_json['eventos'][] = $evento;

    endforeach;

    return json_encode($eventos_json);
}

function url_completa($forwarded_host = false){
    $ssl   = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on';
    $proto = strtolower($_SERVER['SERVER_PROTOCOL']);
    $proto = substr($proto, 0, strpos($proto, '/')) . ($ssl ? 's' : '' );
    if ($forwarded_host && isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
        $host = $_SERVER['HTTP_X_FORWARDED_HOST'];
    } else {
        if (isset($_SERVER['HTTP_HOST'])) {
            $host = $_SERVER['HTTP_HOST'];
        } else {
            $port = $_SERVER['SERVER_PORT'];
            $port = ((!$ssl && $port=='80') || ($ssl && $port=='443' )) ? '' : ':' . $port;
            $host = $_SERVER['SERVER_NAME'] . $port;
        }
    }
    $request = $_SERVER['REQUEST_URI'];
    return $proto . '://' . $host . $request;
}
?>

    <footer class="site-footer">
      <div class="contenedor clearfix">
        <div class="footer-informacion">
          <h3>Sobre <span>gdlwebbcamp</span></h3>
          <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dicta, excepturi debitis perferendis doloribus ipsam rerum inventore ipsum reiciendis, fuga sapiente est velit, recusandae accusantium. Consectetur architecto ipsa cumque dolor quo!</p>
        </div>
        <div class="ultimos-tweets">
          <h3>Ultimos <span>tweets</span></h3>
          <ul>
            <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla sint sequi rerum quisquam eligendi praesentium</li>
            <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla sint sequi rerum quisquam eligendi praesentium</li>
            <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla sint sequi rerum quisquam eligendi praesentium</li>
          </ul>
        </div>
        <div class="menu">
          <h3>Redes <span>sociales</span></h3>
          <nav class="redes-sociales">
            <a href=""><i class="fab fa-facebook-f"></i></a>
            <a href=""><i class="fab fa-twitter"></i></a>
            <a href=""><i class="fab fa-pinterest"></i></a>
            <a href=""><i class="fab fa-youtube"></i></a>
            <a href=""><i class="fab fa-instagram"></i></a>
          </nav>
        </div>
      </div>
      <p class="copyright">Todos los derechos Reservados GDLWEBCAMP 2021&copy;</p>
    </footer>
    
  <script src="leaflet/leaflet.js"></script>
  <script src="js/vendor/modernizr-3.11.2.min.js"></script>

  <script src="js/plugins.js"></script>
  <script src="js/jquery3.6.0.js"></script>
  <script src="js/jquerymin3.6.0.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
    <!--Gallelry-->  
  <?php  
    $archivo = basename($_SERVER['PHP_SELF']);
    $pagina = str_replace(".php", "", $archivo);
    if($pagina == 'invitados' || $pagina == 'index'){
      echo '<script src="js/jquery.colorbox-min.js"></script>';
    }else if($pagina == 'Conferencia'){
      echo '<script src="js/lightgallery-all.min.js"></script>';
      echo '<script src="js/lg-thumbnail.min.js"></script>';
    }
  ?>

  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/jquery.lettering-0.6.1.min.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  
  <script src="js/main.js"></script>

  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set', 'anonymizeIp', true); ga('set', 'transport', 'beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
  
</body>

</html>
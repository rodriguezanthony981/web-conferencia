<!doctype html>
<html class="no-js" lang="es">

<head>
  <meta charset="utf-8">
  
  <title></title>
  <meta name="description" content="Conferencia">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <meta property="og:title" content="">
  <meta property="og:type" content="">
  <meta property="og:url" content="">
  <meta property="og:image" content="">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/all.css"><!--Iconos FontAwesome-->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Oswald:wght@700&family=PT+Sans&display=swap" rel="stylesheet">
  
  <?php 
    $archivo = basename($_SERVER['PHP_SELF']);
    $pagina = str_replace(".php", "", $archivo);
    if($pagina == 'invitados' || $pagina == 'index'){
      echo '<link rel="stylesheet" href="css/colorbox.css">';
    }else if($pagina == 'Conferencia'){
      echo '<link rel="stylesheet" href="css/lightgallery.css">';
    }
  ?>
  <link rel="stylesheet" href="leaflet/leaflet.css" />
  <link rel="stylesheet" href="css/main.css">

  <meta name="theme-color" content="#fafafa">
</head>

<body class="<?php echo $pagina; ?>">
  
    <header class="site-header">
      <div class="hero">
        <div class="contenido-header">
          <nav class="redes-sociales">
            <a href="https://www.facebook.com" target="_blank"><i class="fab fa-facebook-f"></i></a>
            <a href="https://twitter.com/?lang=es" target="_blank"><i class="fab fa-twitter"></i></a>
            <a href="https://www.pinterest.es" target="_blank"><i class="fab fa-pinterest"></i></a>
            <a href="https://www.youtube.com" target="_blank"><i class="fab fa-youtube"></i></a>
            <a href="https://www.instagram.com/?hl=es" target="_blank"><i class="fab fa-instagram"></i></a>
          </nav>
          <div class="informacion-evento">
            <div class="clearfix">
              <p class="fecha"><i class="far fa-calendar-alt"></i>10 - 12 Dic</p>
              <p class="ciudad"><i class="fas fa-map-marker-alt"></i>Guadalajara, MX</p>
            </div>
            <h1 class="nombre-sitio">GDLWebCamp</h1>
            <p  class="slogan">La mejor conferencia de <span>Diseño Web</span></p>
          </div><!--Informacion-evento-->
        </div>
      </div><!--Hero-->
    </header>

    <div class="barra">
      <div class="contenedor clearfix">
        <div class="logo">
          <a href="index.php">
            <img src="img/logo.svg" alt="Logo">
          </a>
        </div>
        <div class="menu-mobil">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <nav class="navegacion-principal">
          <a href="Conferencia.php">Conferencia</a>
          <a href="calendario.php">Calendario</a>
          <a href="invitados.php">Invitados</a>
          <a href="registro.php">Reservaciones</a>
        </nav>
      </div>
    </div><!--Cierre Barra-->
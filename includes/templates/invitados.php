   
    
<section id="Invitados_Sec" class="seccion  contenedor">
    
    <h2>Nuestros Invitados</h2>

    <?php 
        try{
            require_once('includes/funciones/db_conexion.php');
            $sql = "SELECT * FROM `invitados` ORDER BY invitado_id ASC";
            $resultado = $conn->query($sql);

        }catch(\Exception $e){
           $error = $e->getMessage();
        }
    ?>
    <section class="invitados contenedor seccion">
        <ul class="lista-invitados clearfix">
            <?php while($invitados = $resultado->fetch_assoc() ){ ?>                        
                <li>
                    <div class="invitados">
                        <a class="invitado-info" href="#invitado<?php echo $invitados['invitado_id']; ?>">
                            <img src="img/<?php echo $invitados['url']?>" alt="invitado-1">
                            <p><?php echo $invitados['nombre_invitado'] . " " . $invitados['apellido_invitado']?></p>
                        </a>
                    </div>
                </li>
                <div style="display:none">
                    <div class="invitado-info" id="invitado<?php echo $invitados['invitado_id'];?>">
                        <h2><?php echo $invitados['nombre_invitado'] . " " . $invitados['apellido_invitado']; ?></h2>
                        <img src="img/<?php echo $invitados['url']?>" alt="invitado-1">
                        <p><?php echo $invitados['descripción']?></p>
                    </div>
                </div>    
            <?php } ?>
        </ul>
    </section>
    
    <?php 
        $conn->close();
    ?>

</section>
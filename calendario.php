<?php include_once 'includes/templates/header.php'; ?>


<section class="seccion contenedor">

    <h2>Calendario de Eventos</h2>
    
    <?php
        try {
            require_once('includes/funciones/db_conexion.php');
            $sql = "SELECT evento_id,nombre_evento,fecha_evento,hora_evento,cat_evento,icono,nombre_invitado,apellido_invitado ";
            $sql .= " FROM eventos";
            $sql .= " INNER JOIN categoria_evento ";
            $sql .= " ON eventos.id_cat_evento = categoria_evento.id_categoria";
            $sql .= " INNER JOIN invitados ";
            $sql .= " ON eventos.id_inv = invitados.invitado_id";
            $sql .= " ORDER BY evento_id ";
            $resultado = $conn->query($sql);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    ?>

    <div class="calendario">
    <?php  $calendario = array();
        while ($eventos = $resultado->fetch_assoc()) {
            $fecha = $eventos['fecha_evento'];
            $categoria = $eventos['cat_evento'];
            $evento = array(
                'titulo' => $eventos['nombre_evento'],
                'fecha' => $fecha,
                'hora' => $eventos['hora_evento'],
                'categoria' => $categoria,
                'icono' => 'fas ' . $eventos['icono'],
                'invitado' => $eventos['nombre_invitado'] . ' ' . $eventos['apellido_invitado']
        );
            $calendario[$fecha][] = $evento; ?>
    <?php
        } #se acaba el While de fetch_assoc?>

    <?php
        #Todos los Eventos
        $dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
        foreach ($calendario as $dia => $lista_eventos) { ?>
            <h3>
                <i class="far fa-calendar-alt"></i>
                <?php
                //UNIX
                setlocale(LC_TIME, 'es_ES.UTF-8');
                //Windows
                setlocale(LC_TIME, 'spanish');

                echo ucwords($dias[strftime("%w", strtotime($dia))]) . ", "  .strftime("%d de %B del %Y", strtotime($dia)); ?>
                <br>
            </h3>
            <?php foreach ($lista_eventos as $evento) { ?>
            <div class="dia">
                <p class="titulo"><?php echo $evento['titulo']?> </p>
                <p class="hora"><i class="far fa-clock" aria-hidden="true"></i>
                    <?php echo $evento['fecha'] . "  " . $evento['hora']?>
                </p>
                <p>
                    <i class="<?php echo $evento['icono']?>" aria-hidden="true"></i>
                    <?php echo $evento['categoria'] ?>
                </p>
                <p><i class="fas fa-user" aria-hidden="true"></i>
                    <?php echo $evento['invitado']?>
                </p>
            </div>

            <?php } // Fin FOR EACH de Eventos?>

    <?php } // FOR EACH DE DIAS?>
    </div><!--Fin Calendario-->

    <pre><?php //var_dump($evento);?></pre>

    <?php
        $conn->close();
    ?>

</section>

<?php include_once 'includes/templates/footer.php' ?>

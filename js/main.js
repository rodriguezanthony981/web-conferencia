(function () {
      'use strict'
      var regalo = document.getElementById('regalo');

      document.addEventListener('DOMContentLoaded', function () {
        var maps = document.getElementById('mapa');
        if (maps != null) {
          var map = L.map('mapa').setView([-2.153816, -79.913419], 17);

          L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              attribution:
              '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);

          L.marker([-2.153816, -79.913419]).addTo(map)
              .bindPopup('WebCamp')
              .openPopup();
        }

        //Campos Datos Usuarios
        var nombre = document.getElementById('nombre');
        var apellido = document.getElementById('apellido');
        var email = document.getElementById('email');

        //Campos Pases
        var paseDia = document.getElementById('pase_dia');
        var paseDosDias = document.getElementById('pase_dosdia');
        var paseCompleto = document.getElementById('pase_completo');

        //Botones y divs
        var calcular = document.getElementById('calcular');
        var errorDiv = document.getElementById('error-text');
        var error = document.getElementById('error');
        var botonRegistro = document.getElementById('btnregistro');
        var listaProductos = document.getElementById('lista-productos');
        var suma = document.getElementById('suma-total');

        //Extras

        var etiquetas = document.getElementById('etiquetas');
        var camisas = document.getElementById('camisa_evento');
        var i = document.getElementById('i');

        if (calcular != null) {
          botonRegistro.disabled = true;

          calcular.addEventListener('click', calcularMontos);
          paseDia.addEventListener('blur', mostrarDias);
          paseDosDias.addEventListener('blur', mostrarDias);
          paseCompleto.addEventListener('blur', mostrarDias);
          nombre.addEventListener('blur', Validador);
          apellido.addEventListener('blur', Validador);
          email.addEventListener('blur', Validador);

          email.addEventListener('blur', validarEmail);
          console.log('%cEste es el Registro', 'color:green;font-family:monospace; font-size: 20px'
          );
        }

        function Validador() {
          if (this.value == '') {
            error.style.display = 'grid';
            i.classList.add('fas', 'fa-exclamation-triangle');
            errorDiv.style.color = 'red';
            errorDiv.innerHTML = 'Este campo es obligatorio';
            this.style.border = '2px solid red';
          } else {
            this.style.border = '1px solid #cccccc';
            error.style.display = 'none';
          }
        }

        function validarEmail() {
          console.log(email.value.length);
          if (email.value.length > 0) {
            console.log('estos es email.value ' + email.value);
            if (this.value.indexOf('@') > -1) {
              this.style.border = '1px solid #cccccc';
              error.style.display = 'none';
            } else {
              error.style.display = 'grid';
              i.classList.add('fas', 'fa-exclamation-triangle');
              errorDiv.style.color = 'red';
              errorDiv.innerHTML = 'No es un Correo Valido';
              this.style.border = '2px solid red';
            }
          }
        }

        function calcularMontos(event) {

          event.preventDefault();
          if (regalo.value === '') {
            alert('Debes elegir un regalo');
            regalo.focus();
          } else {
            var boletosDia = parseInt(paseDia.value, 10) || 0;
            var boletos2Dias = parseInt(paseDosDias.value, 10) || 0;
            var BoletosCompletos = parseInt(paseCompleto.value, 10) || 0;
            var cantCamisas = parseInt(camisas.value, 10) || 0;
            var cantEtiquetas = parseInt(etiquetas.value, 10) || 0;

            var totalpagar = (boletosDia * 30) + (boletos2Dias * 45) +
            (BoletosCompletos * 50) + ((cantCamisas * 10) * .93) + (cantEtiquetas * 2);

            var listadoProductos = [];

            if (boletosDia >= 1) {
              listadoProductos.push(boletosDia + ' Pases por Día');
            }

            if (boletos2Dias >= 1) {
              listadoProductos.push(boletos2Dias + ' Pases por 2 Días');
            }

            if (BoletosCompletos >= 1) {
              listadoProductos.push(BoletosCompletos + ' Pases Completos');
            }

            if (cantCamisas >= 1) {
              listadoProductos.push(cantCamisas + ' Camisas');
            }

            if (cantEtiquetas >= 1) {
              listadoProductos.push(cantEtiquetas + ' Etiquetas');
            }

            listaProductos.innerHTML = '';

            if (totalpagar != 0) {
              listaProductos.style.padding = '20px';

            } else {
              listaProductos.style.padding = '0px';

            }

            for (var i = 0; i < listadoProductos.length; i++) {
              listaProductos.innerHTML += listadoProductos[i] + '<br/>';
            }

            suma.innerHTML = '$' + totalpagar.toFixed(2);
            console.log(listadoProductos);

            botonRegistro.disabled = false;
            document.getElementById('total_pedido').value = totalpagar;
          }
        }

        function mostrarDias() {
          var boletosDia = parseInt(paseDia.value, 10) || 0;
          var boletos2Dias = parseInt(paseDosDias.value, 10) || 0;
          var BoletosCompletos = parseInt(paseCompleto.value, 10) || 0;

          var diasElegidos = [];
          if (boletosDia > 0) {
            diasElegidos.push('viernes');
          }

          if (boletos2Dias > 0) {
            diasElegidos.push('viernes', 'sabado');
          }

          if (BoletosCompletos > 0) {
            diasElegidos.push('viernes', 'sabado', 'domingo');
          }

          for (var i = 0; i < diasElegidos.length; i++) {
            document.getElementById(diasElegidos[i]).style.display = 'block';
          }
        }

        console.log('listo');

        //Color-Box
        if (document.getElementById('Invitados_Sec')) {
          $('.invitado-info').colorbox({ inline: true, width: '55%' });
        }

      }); // DOM Loaded
    })();

$(function () {
    //Menu Scroll
    var windowsHight = $(window).height();
    var barraAltura = $('.barra').innerHeight();
    console.log(barraAltura);
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll > windowsHight) {
          $('.barra').addClass('fixed');
          $('body').css({ 'margin-top': barraAltura + 'px' });
        } else {
          $('body').css({ 'margin-top': '0px' });
          $('.barra').removeClass('fixed');
        }
      });

    //Menu Responsive
    $('.menu-mobil').on('click', function () {
        $('.navegacion-principal').slideToggle();
      });

    //Lettering
    $('.nombre-sitio').lettering();

    $('div.ocultar').hide();
    $('.menu-programa a:first').addClass('activo');
    $('.programa-evento .info-curso:first').show();
    $('.menu-programa a').on('click', function () {
        $('.menu-programa a').removeClass('activo');
        $(this).addClass('activo');
        $('.ocultar').hide();

        var enlace = $(this).attr('href');
        $(enlace).fadeIn(1000);
        return false;
      });

    // Agregar Clase a Menu
    $('body.Conferencia .navegacion-principal a:contains("Conferencia")').addClass('activo');
    $('body.calendario .navegacion-principal a:contains("Calendario")').addClass('activo');
    $('body.invitados .navegacion-principal a:contains("Invitados")').addClass('activo');

    //Solo En el Sitio Principal

    if (document.getElementById('Resumen')) {
      //Animaciones Numeros
      var waypoint = new Waypoint({
          element: document.getElementById('Resumen'),
          handler: function () {
              $('.resumen-evento li:nth-child(1) p').animateNumber({ number: 6 }, 1200);
              $('.resumen-evento li:nth-child(2) p').animateNumber({ number: 15 }, 1200);
              $('.resumen-evento li:nth-child(3) p').animateNumber({ number: 3 }, 600);
              $('.resumen-evento li:nth-child(4) p').animateNumber({ number: 9 }, 1500);
            }, offset: '50%'
        });

      //Cuenta Regresiva

      $('.cuenta-regresiva').countdown('2021/08/16 01:00:00', function (event) {
          $('#dias').html(event.strftime('%D'));
          $('#horas').html(event.strftime('%H'));
          $('#minutos').html(event.strftime('%M'));
          $('#segundos').html(event.strftime('%S'));
        });

      console.log('%cEsta pagina es la Principal',
      'color:green;font-family:monospace; font-size: 20px');



    }

    if (document.getElementById('aniimated-thumbnials')) {
      $('#aniimated-thumbnials').lightGallery({
        thumbnail: true,
      });
      let num = 11;
      let bool = true;
      console.log(bool);
      tamaño();
      $(window).resize(function () {
          bool = false;
          tamaño();
        });

      function tamaño() {
        if (bool == true) {
          if (800 <= screen.width && screen.width <= 1320) {
            num = 9;
          } else if (screen.width >= 1320) {
            num = 11;
          }else if (800 > screen.width) {
            num = 7;
          }

          for (num; num <= 21; num++) {
            var ocultar = '#aniimated-thumbnials a:nth-child(' + num + ')';
            $(ocultar).hide();
          }
        }else if (800 <= screen.width && screen.width <= 1320) {
          for (num = 7; num <= 9; num++) {
            let ocultar = '#aniimated-thumbnials a:nth-child(' + num + ')';
            $(ocultar).show();
          }

          for (num = 9; num <= 11; num++) {
            let ocultar = '#aniimated-thumbnials a:nth-child(' + num + ')';
            $(ocultar).hide();
          }
        }else if (screen.width >= 1320) {
          for (num = 7; num < 11; num++) {
            let ocultar = '#aniimated-thumbnials a:nth-child(' + num + ')';
            $(ocultar).show();
          }
        }else if (800 > screen.width) {
          for (num = 7; num < 11; num++) {
            let ocultar = '#aniimated-thumbnials a:nth-child(' + num + ')';
            $(ocultar).hide();
          }
        }
      }

      console.log('%cEsta pagina es la de Conferencia',
      'color:green;font-family:monospace; font-size: 20px');
    }

  });

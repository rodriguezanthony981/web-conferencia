<?php include_once 'includes/templates/header.php'; ?>

    <section class="seccion contenedor">
      <h2>La mejor conferencia de diseño web en español</h2>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique eaque odio, veniam a consequatur deserunt quam tempore quisquam. Dolorem pariatur porro voluptates esse doloremque vitae, dolore nulla veritatis quae laudantium!</p>
    </section> <!-- SECTION  -->

    <section class="programa">
      <div class="contenedor-video">
        <video autoplay loop poster="img/bg-talleres.jpg">
          <source src="img/video/video.mp4" type="video/mp4">
          <source src="img/video/video.webm" type="video/webm">
          <source src="img/video/video.ogv" type="video/ogv">
        </video>
      </div><!--Contenedor video-->
      <div class="contenido-programa">
        <div class="contenedor">
          <div class="programa-evento">
            <h2>Programa del Evento</h2>
            <?php 
              try{
                  require_once('includes/funciones/db_conexion.php');
                  $sql = "SELECT * FROM `categoria_evento`";
                  $sql .= "ORDER BY id_categoria DESC";
                  $resultado = $conn->query($sql);
              }catch(\Exception $e){
                  echo $e->getMessage();
              }
            ?>
            
            <nav class="menu-programa">
            
            <?php while($cat = $resultado->fetch_array(MYSQLI_ASSOC)){ ?>
            
            <?php 
              $categoria = $cat['cat_evento']; 
              $icon = $cat['icono']; 
              ?>
              <?php #var_dump($cat);?>
              <a href="#<?php echo trim(strtolower($categoria)); ?>">
                <i class="fas <?php echo $icon;?>"aria-hidden="true" ></i> <?php echo $categoria;?></a>
            <?php }?>
            </nav>

            <?php 
              try{
                  require_once('includes/funciones/db_conexion.php');
                  $sql = "  SELECT evento_id,nombre_evento,fecha_evento,hora_evento,cat_evento,icono,nombre_invitado,apellido_invitado ";
                  $sql .= " FROM eventos";
                  $sql .= " INNER JOIN categoria_evento ";
                  $sql .= " ON eventos.id_cat_evento = categoria_evento.id_categoria";
                  $sql .= " INNER JOIN invitados ";
                  $sql .= " ON eventos.id_inv = invitados.invitado_id";
                  $sql .= " AND eventos.id_cat_evento = 1";
                  $sql .= " ORDER BY evento_id LIMIT 2;";
                  $sql .= " SELECT evento_id,nombre_evento,fecha_evento,hora_evento,cat_evento,icono,nombre_invitado,apellido_invitado ";
                  $sql .= " FROM eventos";
                  $sql .= " INNER JOIN categoria_evento ";
                  $sql .= " ON eventos.id_cat_evento = categoria_evento.id_categoria";
                  $sql .= " INNER JOIN invitados ";
                  $sql .= " ON eventos.id_inv = invitados.invitado_id";
                  $sql .= " AND eventos.id_cat_evento = 2";
                  $sql .= " ORDER BY evento_id LIMIT 2;";
                  $sql .= " SELECT evento_id,nombre_evento,fecha_evento,hora_evento,cat_evento,icono,nombre_invitado,apellido_invitado ";
                  $sql .= " FROM eventos";
                  $sql .= " INNER JOIN categoria_evento ";
                  $sql .= " ON eventos.id_cat_evento = categoria_evento.id_categoria";
                  $sql .= " INNER JOIN invitados ";
                  $sql .= " ON eventos.id_inv = invitados.invitado_id";
                  $sql .= " AND eventos.id_cat_evento = 3";
                  $sql .= " ORDER BY evento_id LIMIT 2;";

              }catch(\Exception $e){
                  echo $e->getMessage();
              }
            ?>
            <?php $conn->multi_query($sql); ?>
            <?php 
              do{
                $resultado = $conn -> store_result();
                $row = $resultado -> fetch_all(MYSQLI_ASSOC); ?>
                
                <?php $i = 0; ?>
                <?php foreach($row as $evento): ?>
                  <?php if($i % 2 == 0) { ?>
                    <div id="<?php echo strtolower($evento['cat_evento']);?>" class="info-curso ocultar clearfix">
                  <?php }?>                      
                      <div class="detalle-evento">
                        <h3><?php echo utf8_decode(utf8_encode($evento['nombre_evento']))?></h3>
                        <p><i class="far fa-clock" aria-hidden="true"></i><?php echo $evento['hora_evento']; ?></p>
                        <p><i class="far fa-calendar-alt" aria-hidden="true"></i><?php echo $evento['fecha_evento']; ?></p>
                        <p><i class="fas fa-user" aria-hidden="true"></i><?php echo $evento['nombre_invitado'] . " ". $evento['apellido_invitado']; ?></p>
                      </div>
                    
                    <?php if($i % 2 == 1): ?>  
                      <a href="calendario.php" class=" button float-right">Ver todos</a>
                    </div><!--talleres-->  
                    <?php endif; ?>
                  <?php $i++; ?>
                <?php endforeach; ?>
                <?php $resultado->free(); ?>
              <?php } while ($conn->more_results() && $conn->next_result());?>
          </div><!--programa-evento-->
        </div><!--contenedor-->
      </div><!--contenidido-programa-->
    </section><!--Programa-->
    <?php include_once 'includes/templates/invitados.php' ?><!---->
    <div id="Resumen"class="contador parallax">
      <div class="contenedor">
        <ul class="resumen-evento clearfix">
          <li><p class="numero">6</p> Invitados</li>
          <li><p class="numero">15</p> Talleres</li>
          <li><p class="numero">8</p> Días</li>
          <li><p class="numero">12</p> Conferencias</li>
        </ul>
      </div>
    </div>
    <section class="precios section">
      <h2>Precios</h2>
      <div class="contenedor">
        <ul class="lista-precios clearfix">
          <li>
            <div class="tabla-precios">
              <h3>Pase por día</h3>
              <p class="numero">$30</p>
              <ul>
                <li>Bocadillos Gratis</li>
                <li>Todas las Conferencias</li>
                <li>Todos los talleres</li>
              </ul>
              <a href="#" class="button hollow">Comprar</a>
            </div>
          </li>
          <li>
            <div class="tabla-precios">
              <h3>Pase Todos los días</h3>
              <p class="numero">$50</p>
              <ul>
                <li>Bocadillos Gratis</li>
                <li>Todas las Conferencias</li>
                <li>Todos los talleres</li>
              </ul>
              <a href="#" class="button ">Comprar</a>
            </div>
          </li>
          <li>
            <div class="tabla-precios">
              <h3>Pase por 2 día</h3>
              <p class="numero">$45</p>
              <ul>
                <li>Bocadillos Gratis</li>
                <li>Todas las Conferencias</li>
                <li>Todos los talleres</li>
              </ul>
              <a href="#" class="button hollow">Comprar</a>
            </div>
          </li>
        </ul>
      </div>  
    </section>
    <div id="mapa" class="mapa"></div>
    <section class="seccion">
      <h2>Testimoniales</h2>
      <div class="testimoniales contenedor clearfix">
        <div class="testimonial">
          <blockquote>
            <p>Consequuntur magni officia nemo ea distinctio quisquam possimus velit quo quam labore. Ratione, animi sapiente.</p>
            <footer class="info-testimonial clearfix">
              <img src="img/testimonial.jpg" alt="Imagen testimonial">
              <cite>Oswaldo Aponte Escobedo <span>Diseñador en @Prisma</span></cite>
            </footer>
          </blockquote>
        </div><!--Testimonial-->
        <div class="testimonial">
          <blockquote>
            <p>Consequuntur magni officia nemo ea distinctio quisquam possimus velit quo quam labore. Ratione, animi sapiente.</p>
            <footer class="info-testimonial clearfix">
              <img src="img/testimonial.jpg" alt="Imagen testimonial">
              <cite>Oswaldo Aponte Escobedo <span>Diseñador en @Prisma</span></cite>
            </footer>
          </blockquote>
        </div><!--Testimonial-->
        <div class="testimonial">
          <blockquote>
            <p>Consequuntur magni officia nemo ea distinctio quisquam possimus velit quo quam labore. Ratione, animi sapiente.</p>
            <footer class="info-testimonial clearfix">
              <img src="img/testimonial.jpg" alt="Imagen testimonial">
              <cite>Oswaldo Aponte Escobedo <span>Diseñador en @Prisma</span></cite>
            </footer>
          </blockquote>
        </div><!--Testimonial-->
      </div>
    </section>
    <div class="newsletter parallax">
      <div class="contenido contenedor">
        <p  >Regístrate al newsletter:</p>
        <h3>GDLWebCamp</h3>
        <a href="#" class="button transparente">Registro</a>
      </div><!--Contenido-->
    </div><!--newsletteer-->
    <section class="seccion">
      <h2>Faltan</h2>
      <div class="cuenta-regresiva contenedor">
        <ul class="clearfix">
          <li><p id="dias" class="numero"></p>días</li>
          <li><p id="horas"class="numero"></p>horas</li>
          <li><p id="minutos" class="numero"></p>minutos</li>
          <li><p id="segundos" class="numero"></p>segundos</li>
        </ul>
      </div>
    </section>
    
<?php include_once 'includes/templates/footer.php' ?>

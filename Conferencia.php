<?php include_once 'includes/templates/header.php'; ?>

    <section class="seccion contenedor">
        <h2>La mejor conferencia de diseño web en español</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, modi enim hic vitae accusantium deleniti fugit repudiandae voluptates aliquid sunt totam odio est autem voluptatibus tenetur qui, molestiae unde inventore!</p>
    </section>

    <section class="seccion contenedor">
        <h2>Galería de Fotos</h2>
        
        <div class="galeria" id="aniimated-thumbnials">
            <a href="img/galeria/01.jpg">
                <img src="img/galeria/thumbs/01.jpg"/>
            </a>
            <a href="img/galeria/02.jpg">
                <img src="img/galeria/thumbs/02.jpg"/>
            </a>
            <a href="img/galeria/03.jpg">
                <img src="img/galeria/thumbs/03.jpg"/>
            </a>
            <a href="img/galeria/04.jpg">
                <img src="img/galeria/thumbs/04.jpg"/>
            </a>
            <a href="img/galeria/05.jpg">
                <img src="img/galeria/thumbs/05.jpg"/>
            </a>
            <a href="img/galeria/06.jpg">
                <img src="img/galeria/thumbs/06.jpg"/>
            </a>
            <a href="img/galeria/07.jpg">
                <img src="img/galeria/thumbs/07.jpg"/>
            </a>
            <a href="img/galeria/08.jpg">
                <img src="img/galeria/thumbs/08.jpg"/>
            </a>
            <a href="img/galeria/09.jpg">
                <img src="img/galeria/thumbs/09.jpg"/>
            </a>
            <a href="img/galeria/10.jpg">
                <img src="img/galeria/thumbs/10.jpg"/>
            </a>
            <a href="img/galeria/11.jpg">
                <img src="img/galeria/thumbs/11.jpg"/>
            </a>
            <a href="img/galeria/12.jpg">
                <img src="img/galeria/thumbs/12.jpg"/>
            </a>
            <a href="img/galeria/13.jpg">
                <img src="img/galeria/thumbs/13.jpg"/>
            </a>
            <a href="img/galeria/14.jpg">
                <img src="img/galeria/thumbs/14.jpg"/>
            </a>
            <a href="img/galeria/15.jpg">
                <img src="img/galeria/thumbs/15.jpg"/>
            </a>
            <a href="img/galeria/16.jpg">
                <img src="img/galeria/thumbs/16.jpg"/>
            </a>
            <a href="img/galeria/17.jpg">
                <img src="img/galeria/thumbs/17.jpg"/>
            </a>
            <a href="img/galeria/18.jpg">
                <img src="img/galeria/thumbs/18.jpg"/>
            </a>
            <a href="img/galeria/19.jpg">
                <img src="img/galeria/thumbs/19.jpg"/>
            </a>
            <a href="img/galeria/20.jpg">
                <img src="img/galeria/thumbs/20.jpg"/>
            </a>
            <a href="img/galeria/21.jpg">
                <img src="img/galeria/thumbs/21.jpg"/>
            </a>
          </div>
    </section>

<?php include_once 'includes/templates/footer.php' ?>
